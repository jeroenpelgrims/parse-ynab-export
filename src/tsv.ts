import Papa from "papaparse";

export function parseString(tsv: string): string[][] {
	return Papa.parse<string[]>(tsv, {
		delimiter: "\t",
		quoteChar: '"',
		trimHeaders: true
	}).data.slice(1);
}

export function extractNumber(text: string) {
	const isNegative = !!text.match(/^-/);
	const match = text.match(/\d+/g);

	return match ? parseInt(match.join("")) * (isNegative ? -1 : 1) : undefined;
}
