import { flatten, groupBy } from "ramda";
import { parse as parseBudget } from "./budget";
import { parse as parseRegister } from "./register";
import { BudgetLine } from "./budget/interfaces";
import {
	RegisterLine,
	RegisterLineType,
	RegisterTransactionLine,
	RegisterSplitLine,
	RegisterTransferLine
} from "./register/interfaces";

export interface YnabData {
	budget: BudgetLine[];
	register: RegisterLine[];
	accounts: string[];
	registerByAccount: { [key: string]: RegisterLine[] };
	categoryHierarchy: Map<string, Set<string>>;
	categories: string[];
	categoryGroups: string[];
}

function getAccounts(register: RegisterLine[]) {
	const accounts = register.map(x => x.account);
	return Array.from(new Set(accounts));
}

function getCategoryHierarchy(budget: BudgetLine[], register: RegisterLine[]) {
	const splitSubLines = flatten<RegisterTransactionLine | RegisterTransferLine>(
		register
			.filter(x => x.type === RegisterLineType.Split)
			.map(x => (x as RegisterSplitLine).splits)
	);

	const transactions = [...register, ...splitSubLines].filter(
		x => x.type === RegisterLineType.Transaction
	) as RegisterTransactionLine[];

	const categoryGroupsAndCategories = [...transactions, ...budget].reduce(
		(result, next) => {
			if (!result.has(next.categoryGroup)) {
				result.set(next.categoryGroup, new Set<string>());
			}
			result.get(next.categoryGroup)!.add(next.category);
			return result;
		},
		new Map<string, Set<string>>()
	);

	return categoryGroupsAndCategories;
}

export function parse(budgetData: string, registerData: string): YnabData {
	const budget = parseBudget(budgetData);
	const register = parseRegister(registerData);
	const categoryHierarchy = getCategoryHierarchy(budget, register);
	const categories = flatten<string>(
		Array.from(categoryHierarchy.values()).map(x => Array.from(x))
	);

	return {
		budget,
		register,
		accounts: getAccounts(register),
		registerByAccount: groupBy<RegisterLine>(x => x.account)(register),
		categoryHierarchy,
		categories,
		categoryGroups: Array.from(categoryHierarchy.keys())
	};
}
