import fs from "fs";
import moment from "moment";
import * as register from "..";
import {
	RegisterLineType,
	RegisterTransactionLine,
	RegisterTransferLine,
	RegisterSplitLine,
	ClearState,
	RegisterLine
} from "../interfaces";

function readTestData<T extends RegisterLine>(file: string) {
	const tsvData = fs.readFileSync(file).toString();
	return register.parse(tsvData) as T[];
}

describe("parse", () => {
	describe("base line", () => {
		const result = readTestData<RegisterTransactionLine>(
			__dirname + "/data/transaction.tsv"
		)[0];

		test("account", () => {
			expect(result.account).toBe("💵 Checking");
		});

		test("flag", () => {
			expect(result.flag).toBe("dummy");
		});

		test("date", () => {
			expect(result.date.toISOString()).toBe(
				moment("2019-07-02").toISOString()
			);
		});

		test("payee", () => {
			expect(result.payee).toBe("Hetzner");
		});

		test("memo", () => {
			expect(result.memo).toBe("test memo");
		});

		test("outflow", () => {
			expect(result.outflow).toBe(3000);
		});

		test("inflow", () => {
			expect(result.inflow).toBe(0);
		});

		test("cleared", () => {
			expect(result.cleared).toBe(ClearState.Uncleared);
		});
	});

	describe("transaction", () => {
		const result = readTestData<RegisterTransactionLine>(
			__dirname + "/data/transaction.tsv"
		)[0];

		test("type", () => {
			expect(result.type).toBe(RegisterLineType.Transaction);
		});

		test("categoryAndGroup", () => {
			expect(result.categoryAndGroup).toBe("Software Subscriptions: Hetzner");
		});

		test("categoryGroup", () => {
			expect(result.categoryGroup).toBe("Software Subscriptions");
		});

		test("category", () => {
			expect(result.category).toBe("Hetzner");
		});
	});

	describe("transfer", () => {
		const result = readTestData<RegisterTransferLine>(
			__dirname + "/data/transfer.tsv"
		)[0];

		test("type", () => {
			expect(result.type).toBe(RegisterLineType.Transfer);
		});

		test("payee", () => {
			expect(result.payee).toBe("💵 Checking");
		});
	});

	describe("split", () => {
		const result = readTestData<RegisterSplitLine>(
			__dirname + "/data/split.tsv"
		)[0];

		test("type", () => {
			expect(result.type).toBe(RegisterLineType.Split);
		});

		test("account", () => {
			expect(result.account).toBe("💵 Checking");
		});

		test("date", () => {
			expect(result.date.toISOString()).toBe(
				moment("2019-06-22").toISOString()
			);
		});

		test("cleared", () => {
			expect(result.cleared).toBe(ClearState.Uncleared);
		});

		test("splits", () => {
			expect(result.splits.length).toBe(2);

			const [split1, split2] = result.splits;

			expect(split1.type).toBe(RegisterLineType.Transaction);
			expect(split2.type).toBe(RegisterLineType.Transfer);

			expect(split1.date.toISOString()).toBe(
				moment("2019-06-22").toISOString()
			);
			expect(split2.date.toISOString()).toBe(
				moment("2019-06-22").toISOString()
			);

			expect(split1.payee).toBe("Spotify");
			expect(split2.payee).toBe("Splitwise Papa");
		});
	});
});
