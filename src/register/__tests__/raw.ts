import fs from "fs";
import { parseString } from "../../tsv";
import { toRawRegisterLine, getLineType } from "../raw";
import { RegisterLineType } from "../interfaces";

const transaction = parseString(
	fs.readFileSync(__dirname + "/data/transaction.tsv").toString()
)[0];
const transfer = parseString(
	fs.readFileSync(__dirname + "/data/transfer.tsv").toString()
)[0];
const split = parseString(
	fs.readFileSync(__dirname + "/data/split.tsv").toString()
)[0];

describe("toRawRegisterLine", () => {
	const result = toRawRegisterLine(transaction);

	test("sets Account correctly", () => {
		expect(result.Account).toBe("💵 Checking");
	});

	test("sets Flag correctly", () => {
		expect(result.Flag).toBe("dummy");
	});

	test("sets Date correctly", () => {
		expect(result.Date).toBe("02/07/2019");
	});

	test("sets Payee correctly", () => {
		expect(result.Payee).toBe("Hetzner");
	});

	test('sets "Category Group/Category" correctly', () => {
		expect(result["Category Group/Category"]).toBe(
			"Software Subscriptions: Hetzner"
		);
	});

	test('sets "Category Group" correctly', () => {
		expect(result["Category Group"]).toBe("Software Subscriptions");
	});

	test('sets "Category" correctly', () => {
		expect(result.Category).toBe("Hetzner");
	});

	test("sets Memo correctly", () => {
		expect(result.Memo).toBe("test memo");
	});

	test("sets Outflow correctly", () => {
		expect(result.Outflow).toBe("kr 30,00");
	});

	test("sets Inflow correctly", () => {
		expect(result.Inflow).toBe("kr 0,00");
	});

	test("sets Cleared correctly", () => {
		expect(result.Cleared).toBe("Uncleared");
	});
});

describe("getLineType", () => {
	test("detects transaction correctly", () => {
		const type = getLineType(toRawRegisterLine(transaction));
		expect(type).toBe(RegisterLineType.Transaction);
	});

	test("detects transfer correctly", () => {
		const type = getLineType(toRawRegisterLine(transfer));
		expect(type).toBe(RegisterLineType.Transfer);
	});

	test("detects split correctly", () => {
		const type = getLineType(toRawRegisterLine(split));
		expect(type).toBe(RegisterLineType.Split);
	});
});
