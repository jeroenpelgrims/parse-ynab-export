import { head, pipe, drop, take } from "ramda";
import moment from "moment";
import * as tsv from "../tsv";
import {
	RegisterLineType,
	RegisterTransactionLine,
	RegisterTransferLine,
	RegisterSplitLine,
	BaseRegisterLine
} from "./interfaces";
import * as raw from "./raw";

function parseBaseRegisterLine(line: raw.RawRegisterLine): BaseRegisterLine {
	return {
		account: line.Account,
		flag: line.Flag,
		date: moment(line.Date, "DD/MM/YYYY"),
		payee: line.Payee,
		memo: line.Memo,
		outflow: tsv.extractNumber(line.Outflow),
		inflow: tsv.extractNumber(line.Inflow),
		cleared: line.Cleared
	};
}

function parseTransactionLine(
	line: raw.RawRegisterLine
): RegisterTransactionLine {
	return {
		...parseBaseRegisterLine(line),
		type: RegisterLineType.Transaction,
		categoryAndGroup: line["Category Group/Category"],
		categoryGroup: line["Category Group"],
		category: line.Category
	};
}

function parseTransferLine(line: raw.RawRegisterLine): RegisterTransferLine {
	const payeePattern = /Transfer : (.*)/;
	const fixedPayee = line.Payee.match(payeePattern)![1];

	return {
		...parseBaseRegisterLine(line),
		type: RegisterLineType.Transfer,
		payee: fixedPayee
	};
}

function parseSplitLine(lines: raw.RawRegisterLine[]): RegisterSplitLine {
	const line = head(lines);
	return {
		type: RegisterLineType.Split,
		account: line.Account,
		date: parseBaseRegisterLine(line).date,
		cleared: line.Cleared,
		splits: (Array.from(parseRawRegisterLines(lines)) as unknown) as (
			| RegisterTransactionLine
			| RegisterTransferLine)[]
	};
}

function findSplitLines(
	line: raw.RawRegisterLine,
	rest: raw.RawRegisterLine[]
) {
	const splitMemoPattern = /Split \(\d+\/(\d+)\) (.*)/;
	const [_, max] = line.Memo.match(splitMemoPattern)!;
	const linesLeft = parseInt(max) - 1;
	const otherSplitLines = take(linesLeft, rest);
	const splitLines = [line, ...otherSplitLines];

	function fixSplitLineMemo(line: raw.RawRegisterLine) {
		const [_1, _2, fixedMemo] = line.Memo.match(splitMemoPattern)!;
		return { ...line, Memo: fixedMemo };
	}

	return splitLines.map(fixSplitLineMemo);
}

function* parseRawRegisterLines(lines: raw.RawRegisterLine[]) {
	let [line, ...rest] = lines;

	while (line !== undefined) {
		const lineType = raw.getLineType(line);

		switch (lineType) {
			case RegisterLineType.Transaction:
				yield parseTransactionLine(line);
				break;
			case RegisterLineType.Transfer:
				yield parseTransferLine(line);
				break;
			case RegisterLineType.Split:
				const splitLines = findSplitLines(line, rest);
				rest = drop(splitLines.length - 1, rest);
				yield parseSplitLine(splitLines);
				break;
		}

		[line, ...rest] = rest;
	}
}

export function parse(registerData: string) {
	const lines: string[][] = tsv.parseString(registerData);
	const rawRegisterLines = lines.map(raw.toRawRegisterLine);
	return Array.from(parseRawRegisterLines(rawRegisterLines));
}
