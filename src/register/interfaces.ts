import moment from 'moment';

export enum ClearState {
	Uncleared = "Uncleared",
	Cleared = "Cleared",
	Reconciled = "Reconciled"
};

export enum RegisterLineType {
	Transfer,
	Transaction,
	Split
}

export interface BaseRegisterLine {
	account: string;
	flag: string;
	date: moment.Moment;
	payee: string;
	cleared: ClearState;
	memo: string;
	outflow: number | undefined;
	inflow: number | undefined;
}

export type RegisterLine =
	| RegisterTransactionLine
	| RegisterTransferLine
	| RegisterSplitLine;
export type RegisterTransactionLine = BaseRegisterLine & {
	type: RegisterLineType.Transaction;
	categoryAndGroup: string;
	categoryGroup: string;
	category: string;
};
export type RegisterTransferLine = BaseRegisterLine & {
	type: RegisterLineType.Transfer;
};
export type RegisterSplitLine = {
	type: RegisterLineType.Split;
	account: string;
	date: moment.Moment;
	cleared: ClearState;
	splits: (RegisterTransactionLine | RegisterTransferLine)[];
};