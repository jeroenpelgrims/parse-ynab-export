import { ClearState, RegisterLineType } from "./interfaces";

export interface RawRegisterLine {
	"Account": string;
	"Flag": string;
	"Date": string;
	"Payee": string;
	"Category Group/Category": string;
	"Category Group": string;
	"Category": string;
	"Memo": string;
	"Outflow": string;
	"Inflow": string;
	"Cleared": ClearState;
}

function isSplitLine(line: RawRegisterLine) {
	return !!line.Memo.match(/^Split/);
}

function isTransferLine(line: RawRegisterLine) {
	return !!line.Payee.match(/^Transfer :/);
}

function isTransactionLine(line: RawRegisterLine) {
	return !(isSplitLine(line) || isTransferLine(line));
}

export function getLineType(line: RawRegisterLine): RegisterLineType {
	if (isSplitLine(line)) {
		return RegisterLineType.Split;
	}

	if (isTransferLine(line)) {
		return RegisterLineType.Transfer;
	}

	if (isTransactionLine(line)) {
		return RegisterLineType.Transaction;
	}

	throw new Error(
		`Can't determine line type for line: "${JSON.stringify(line)}"`
	);
}

export function toRawRegisterLine(line: string[]): RawRegisterLine {
	return {
		"Account": line[0],
		"Flag": line[1],
		"Date": line[2],
		"Payee": line[3],
		"Category Group/Category": line[4],
		"Category Group": line[5],
		"Category": line[6],
		"Memo": line[7],
		"Outflow": line[8],
		"Inflow": line[9],
		"Cleared": line[10] as ClearState
	};
}