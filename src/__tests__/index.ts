import fs from "fs";
import { parse } from "..";
import { RegisterLineType, RegisterSplitLine } from "../register/interfaces";

describe("parse", () => {
	const budgetData = fs
		.readFileSync(__dirname + "/data/budget-mix.tsv")
		.toString();
	const registerData = fs
		.readFileSync(__dirname + "/data/register-mix.tsv")
		.toString();
	const result = parse(budgetData, registerData);

	/*
		budget and register aren't tested here because
		the parsing code is tested in the corresponding modules.
	*/

	test("accounts", () => {
		expect(result.accounts).toHaveLength(2);
		expect(result.accounts[0]).toBe("💵 Checking");
		expect(result.accounts[1]).toBe("💰 Buffer");
	});

	test("registerByAccount", () => {
		const { registerByAccount } = result;
		const keys = Object.keys(registerByAccount);
		const [checkingKey, bufferKey] = keys;

		expect(keys).toHaveLength(2);
		expect(registerByAccount[checkingKey]).toHaveLength(2);
		expect(registerByAccount[bufferKey]).toHaveLength(1);

		expect(registerByAccount[checkingKey][0].type).toBe(
			RegisterLineType.Transaction
		);
		expect(registerByAccount[checkingKey][1].type).toBe(RegisterLineType.Split);

		const split = registerByAccount[checkingKey][1] as RegisterSplitLine;
		expect(split.splits[0].type).toBe(RegisterLineType.Transaction);
		expect(split.splits[1].type).toBe(RegisterLineType.Transfer);

		expect(registerByAccount[bufferKey][0].type).toBe(
			RegisterLineType.Transfer
		);
	});

	test("categoryHierarchy", () => {
		const keys = Array.from(result.categoryHierarchy.keys());

		expect(keys).toHaveLength(2);
		expect(keys).toContain("Essential Expenses");
		expect(keys).toContain("Software Subscriptions");

		const essExp = Array.from(
			result.categoryHierarchy.get("Essential Expenses")!
		);
		expect(essExp).toHaveLength(2);
		expect(essExp).toContain("🍲 Groceries");
		expect(essExp).toContain("⚕️ Medical");

		const softSub = Array.from(
			result.categoryHierarchy.get("Software Subscriptions")!
		);
		expect(softSub).toHaveLength(2);
		expect(softSub).toContain("Hetzner");
		expect(softSub).toContain("Spotify");
	});

	test("categories", () => {
		expect(result.categories).toHaveLength(4);
		expect(result.categories).toContain("Hetzner");
		expect(result.categories).toContain("Spotify");
		expect(result.categories).toContain("🍲 Groceries");
		expect(result.categories).toContain("⚕️ Medical");
	});

	test("categoryGroups", () => {
		expect(result.categoryGroups).toHaveLength(2);
		expect(result.categoryGroups).toContain("Essential Expenses");
		expect(result.categoryGroups).toContain("Software Subscriptions");
	});
});
