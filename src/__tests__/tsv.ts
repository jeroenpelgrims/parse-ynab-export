import fs from "fs";
import { extractNumber, parseString } from "../tsv";

describe("parseString", () => {
	const tsvData = fs
		.readFileSync(__dirname + "/data/register-mix.tsv")
		.toString();

	test("reads correct amount of lines", () => {
		const result = parseString(tsvData);
		expect(result.length).toBe(4);
	});

	test("reads correct amount of columns", () => {
		const result = parseString(tsvData);
		expect(result[0].length).toBe(11);
	});

	test("skips headers", () => {
		const result = parseString(tsvData);
		expect(result[0][0]).not.toBe("Account");
		expect(result[0][0]).toBe("💵 Checking");
	});
});

describe("extractNumber", () => {
	test("0", () => {
		expect(extractNumber("0")).toBe(0);
	});

	test("12,34", () => {
		expect(extractNumber("12,34")).toBe(1234);
	});

	test("12.34", () => {
		expect(extractNumber("12.34")).toBe(1234);
	});

	test("-12.34", () => {
		expect(extractNumber("-12.34")).toBe(-1234);
	});

	test("-€11,51", () => {
		expect(extractNumber("-€11,51")).toBe(-1151);
	});
});
