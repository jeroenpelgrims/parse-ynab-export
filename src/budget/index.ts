import moment from "moment";
import * as tsv from "../tsv";
import { BudgetLine } from "./interfaces";

function toBudgetLine(line: string[]): BudgetLine {
	return {
		month: moment(line[0], "MMM YYYY"),
		categoryAndGroup: line[1],
		categoryGroup: line[2],
		category: line[3],
		budgeted: tsv.extractNumber(line[4]) || 0,
		activity: tsv.extractNumber(line[5]) || 0,
		available: tsv.extractNumber(line[6]) || 0
	};
}

export function parse(budgetData: string) {
	const lines: string[][] = tsv.parseString(budgetData.trim());
	return lines.map(toBudgetLine);
}
