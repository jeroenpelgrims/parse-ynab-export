import fs from "fs";
import moment from "moment";
import * as budget from "..";

describe("parse", () => {
	const budgetData = fs.readFileSync(__dirname + "/data/budget.tsv").toString();
	const result = budget.parse(budgetData);

	test("correct length", () => {
		expect(result.length).toBe(1);
	});

	test("month", () => {
		expect(result[0].month.toISOString()).toBe(moment("2019-07").toISOString());
	});

	test("categoryAndGroup", () => {
		expect(result[0].categoryAndGroup).toBe("Essential Expenses: 🍲 Groceries");
	});

	test("categoryGroup", () => {
		expect(result[0].categoryGroup).toBe("Essential Expenses");
	});

	test("category", () => {
		expect(result[0].category).toBe("🍲 Groceries");
	});

	test("budgeted", () => {
		expect(result[0].budgeted).toBe(15000);
	});

	test("activity", () => {
		expect(result[0].activity).toBe(-1151);
	});

	test("available", () => {
		expect(result[0].available).toBe(13849);
	});
});
