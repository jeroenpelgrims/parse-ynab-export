import moment from 'moment';

export interface BudgetLine {
	month: moment.Moment;
	categoryAndGroup: string;
	categoryGroup: string;
	category: string;
	budgeted: number;
	activity: number;
	available: number;
}