This module will help you convert YNAB's register and budget tsv files to an easy to use data structure.

## Install

```bash
npm install -g parse-ynab-export
```

## Usage

```typescript
import { parse } from "parse-ynab-export";

// This data is the same format as you get from the 2 files in the YNAB export zip
const budgetData = `"Month"	"Category Group/Category"	"Category Group"	"Category"	"Budgeted"	"Activity"	"Available"
"Jun 2019"	"Essential Expenses: Groceries"	"Essential Expenses"	"🍲 Groceries"	€144,36	-€197,44	€0,00
"Jul 2019"	"Essential Expenses: 🍲 Groceries"	"Essential Expenses"	"🍲 Groceries"	€150,00	-€11,51	€138,49`;
const registerData = `"Account"	"Flag"	"Date"	"Payee"	"Category Group/Category"	"Category Group"	"Category"	"Memo"	"Outflow"	"Inflow"	"Cleared"
"💵 Checking"	""	"02/07/2019"	"Hetzner"	"Software Subscriptions: Hetzner"	"Software Subscriptions"	"Hetzner"	""	kr 30,00	kr 0,00	"Uncleared"
"💵 Checking"	""	"22/06/2019"	"Spotify"	"Software Subscriptions: Spotify"	"Software Subscriptions"	"Spotify"	"Split (1/2) "	kr 84,50	kr 0,00	"Uncleared"
"💵 Checking"	""	"22/06/2019"	"Transfer : Splitwise Papa"	""	""	""	"Split (2/2) Spotify"	kr 84,50	kr 0,00	"Uncleared"
"💰 Buffer"	""	"27/02/2019"	"Transfer : 💵 Checking"	""	""	""	"Transfer to buffer from checking"	kr 0,00	kr 1234,44	"Reconciled"`;

parse(budgetData, registerData);
```

This returns an object of type YnabData. (see below for info on the types)

```typescript
Object {
	// All the budget lines (for each year/month and category)
	budget: (2) […],
	// All the transaction lines
	register: (3) […],
	// All the accounts used in the transaction (register) lines
	accounts: (2) […],
	// Register lines grouped by account. (key = account name, value = RegisterLine[])
	registerByAccount: {…},
	// A hierarchy of the category groups and categories
	// (key = category group name, value = array of category names)
	categoryHierarchy: Map(2),
	categories: (3) […],
	categoryGroups: (2) […]
}
```

## Interfaces

### [YnabData interface](https://gitlab.com/jeroenpelgrims/parse-ynab-export/-/blob/master/src/index.ts#L13)

```typescript
interface YnabData {
	budget: BudgetLine[];
	register: RegisterLine[];
	accounts: string[];
	registerByAccount: { [key: string]: RegisterLine[] };
	categoryHierarchy: Map<string, Set<string>>;
	categories: string[];
	categoryGroups: string[];
}
```

### [Budget lines](https://gitlab.com/jeroenpelgrims/parse-ynab-export/blob/master/src/budget/interfaces.ts)

```typescript
interface BudgetLine {
	month: moment.Moment;
	categoryAndGroup: string;
	categoryGroup: string;
	category: string;
	budgeted: number;
	activity: number;
	available: number;
}
```

### [Register lines](https://gitlab.com/jeroenpelgrims/parse-ynab-export/blob/master/src/register/interfaces.ts)

The register lines are split up in to 3 interfaces.

- Regular transaction
- A transfer from 1 account to an other
- A split, when a transaction is split up into multiple categories  
  These splits are bundled up into 1 "split" line with a property "splits" that contains the corresponding split lines.

See register line interfaces [here](https://gitlab.com/jeroenpelgrims/parse-ynab-export/blob/master/src/register/interfaces.ts).
