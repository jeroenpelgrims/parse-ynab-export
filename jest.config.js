module.exports = {
	roots: ["<rootDir>/src"],
	transform: {
		"^.+\\.tsx?$": "ts-jest"
	}
	// setupFiles: ["./testsetup.ts"]
	// globalSetup: "<rootDir>/src/jest/globalSetup.ts",
	// globalTeardown: "<rootDir>/src/jest/globalTeardown.ts"
};
